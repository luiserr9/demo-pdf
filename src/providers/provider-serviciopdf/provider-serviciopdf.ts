import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


/*
  Generated class for the ProviderServiciopdfProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProviderServiciopdfProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ProviderServiciopdfProvider Provider');
  }

  pdf(){
    return this.http.get('https://s3.us-south.cloud-object-storage.appdomain.cloud/us-south-invoices/factura_movistar.pdf');
  }

}
