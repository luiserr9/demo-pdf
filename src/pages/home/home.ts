import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { ProviderServiciopdfProvider } from '../../providers/provider-serviciopdf/provider-serviciopdf'
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { AlertController } from 'ionic-angular';
import { Base64 } from '@ionic-native/base64';
import { AndroidPermissions } from '@ionic-native/android-permissions';

import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  urlpdf:any;
  singleValue:any;
  constructor(public navCtrl: NavController,
    public serpdf:ProviderServiciopdfProvider,
    private transfer: FileTransfer, 
    private file: File,
    private platfrom:Platform,
    public loading:LoadingController,
    public fileOpener:FileOpener,
    public alertCtrl: AlertController,
    public base64:Base64,
    public androidPermissions:AndroidPermissions
    ) {
      this.filePermission();
      
  }

  pdf(){

    
    var url = 'https://saac-api-dev-east.us-east.mybluemix.net/invoices?uniqueId=1079262939&billCycleId=20200309';
    let path = null;

    if (this.platfrom.is('ios')) {
      path= this.file.documentsDirectory;
    }else if (this.platfrom.is('android')) {

      // file:///storage/emulated/0/Download
      path = this.file.externalDataDirectory.split("/A")[0]+"/Download/";
    }else{
      path = null;
    }

    var load= this.loading.create({
      content: "Espere mientras carga el pdf",
    });
    load.present();
     
    if (path != null) {
      const fileTransfer = this.transfer.create();

      console.log("ruta de descarga: "+path);
      

      fileTransfer.download(url, path+"myfile.pdf").then((entry) => {
      //this.urlpdf = entry.toURL();
      console.log('download complete: ' + entry.toURL());
      load.dismiss();

      /*this.base64.encodeFile(entry.toURL()).then(
        (base64File:string)=>{
          console.log("basefile64 android: "+base64File);
          this.navCtrl.push('PagesPdfvistaPage',{pdflocal:entry.toURL(),base64File:base64File});
        },(err)=>{

      });*/
  
  
        
  
      this.openfiel(entry.toURL());
    
  
      }, (error) => {

        console.log("Error de descarga: "+JSON.stringify(error));
        load.dismiss();

        if(error.http_status == 404 && error.body != null  ){
          const alert = this.alertCtrl.create({
            title: 'ERROR!',
            subTitle: 'no se apodido descargar el pdf',
            buttons: ['OK']
          });
          alert.present();
        }else if (error.http_status == 200 && error.body == null ) {
          const alert = this.alertCtrl.create({
            title: 'ERROR!',
            subTitle: '¡ups! la aplicación no tiene permisos de almacenamiento, por favor vaya a configuración->aplicaciones->permisos y active la opción de almacenamiento.',
            buttons: ['OK']
          });
          alert.present();
          
        }

       

        
        
      // handle error
      });

    }else{
      load.dismiss();
      console.log("no se detecto una plataforma movil");
      this.navCtrl.push('PagesPdfvistaPage',{pdflocal:null,base64File:url});

      
    }

  }

  filePermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => console.log('Has permission?', result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );

  }


  base(base){
    this.urlpdf=base;
   

  }


  openfiel(urlfile){
    this.fileOpener.open(urlfile, 'application/pdf')
        .then(() => {console.log('File is opened')})
        .catch(e => {
          console.log('Error opening file', e)
          const alert = this.alertCtrl.create({
            title: 'ERROR!',
            subTitle: 'No se ha encontrado ninguna app para leer pdf´s, instale alguna',
            buttons: ['OK']
          });
          alert.present();

        this.base64.encodeFile(urlfile).then(
        (base64File:string)=>{
          console.log("basefile64 android: "+base64File);
          this.navCtrl.push('PagesPdfvistaPage',{pdflocal:urlfile,base64File:base64File});
        },(err)=>{

        });
          
        });
  }


  toDataURL(url, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onload = function() {
        var fileReader = new FileReader();
        fileReader.onloadend = function() {
          callback(fileReader.result);
        }
        fileReader.readAsDataURL(httpRequest.response);
    };
    httpRequest.open('GET', url);
    httpRequest.responseType = 'blob';
    httpRequest.send();
  }
  


  


}
