import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PDFDocumentProxy, PDFProgressData } from 'pdfjs-dist';

/**
 * Generated class for the PagesPdfvistaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pages-pdfvista',
  templateUrl: 'pages-pdfvista.html',
})
export class PagesPdfvistaPage {
  pdfSrc:any;
  zoompdf = 1;
  localpdf;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.localpdf= this.navParams.get('base64File');
    this.pdfSrc= localStorage.getItem('base');
    console.log("pdf local"+this.localpdf);
    
  }

  ionViewDidLoad() {
    
    console.log('ionViewDidLoad PagesPdfvistaPage');
   
  }


  zoommas(){
   
    this.zoompdf++;
    if (this.zoompdf >5 ) {
      this.zoompdf=5
    }
      

  }
  zoommenos(){
    this.zoompdf--;
   
    if (this.zoompdf < 1) {
      this.zoompdf=1;
    }
    
    
  }

  onAfterLoadComplete(pdf: PDFDocumentProxy) {
    console.log("Termino Carga del  PDF", pdf);
    //this.preload = 2; 
    // do anything with "pdf"
    var viewer = window.document.getElementById("pdfViewer");
    console.log("PDF viewer:", viewer);
    console.log("onAfterLoadComplete", pdf,  pdf.dataLoaded);
    var intervalPdf = setInterval(function(){

      //self.isTimeout = true;
      var pageCache:any= pdf['_transport']['pageCache'];
      if(pageCache != undefined && pageCache.length > 0 ){
        console.log("Ya cargo");
        clearInterval(intervalPdf);
      }
      
      
    }, 200);
    console.log("ACA", pdf['_transport']['pageCache']);
   
  }

  onProgress(progressData: PDFProgressData) {
    console.log("progressData", progressData);
    
  }

  onError(error: any) {
    // do anything
    console.log("Error PDF", error);
  }


}
