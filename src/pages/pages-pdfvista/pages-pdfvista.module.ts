import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagesPdfvistaPage } from './pages-pdfvista';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    PagesPdfvistaPage,
  ],
  imports: [
    PdfViewerModule,
    IonicPageModule.forChild(PagesPdfvistaPage),
  ],
})
export class PagesPdfvistaPageModule {}
